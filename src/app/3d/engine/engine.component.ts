import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EngineService } from './engine.service';

@Component({
  selector: 'app-engine',
  templateUrl: './engine.component.html'
})
export class EngineComponent implements OnInit, OnDestroy {

  @ViewChild('rendererCanvas', {static: true})
  public rendererCanvas: ElementRef<HTMLCanvasElement>;

  public constructor(private s3d: EngineService) { }

  public ngOnInit(): void {
    this.s3d.createScene(this.rendererCanvas);
    this.s3d.animate();
  }

  public ngOnDestroy(): void {
    this.s3d.destroyScene();
  }

}

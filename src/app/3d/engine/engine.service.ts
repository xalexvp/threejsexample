import * as THREE from 'three';
import { Injectable, ElementRef, NgZone } from '@angular/core';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { DragControls } from 'three/examples/jsm/controls/DragControls';

@Injectable({ providedIn: 'root' })
export class EngineService {
  private canvas: HTMLCanvasElement;
  private renderer: THREE.WebGLRenderer;
  private camera: THREE.PerspectiveCamera;
  private scene: THREE.Scene;
  private mouse = new THREE.Vector2();
  private raycaster = new THREE.Raycaster();
  private isNeedControl = true;
  private enableSelection = false;
  private controls: any;
  private group: any;
  private dragControls: any;
  private sceneObjects = [];
  private light: THREE.AmbientLight;
  private lightColor1 = 0x404040;
  private lightPosition1 = 100;
  private cubeSizes1 = [1, 1, 1];
  private sphereSizes1 = [0.65, 32, 32];
  private color1 = 0xffffff;

  private frameId: number = null;

  public constructor(private ngZone: NgZone) {}

  public createScene(canvas: ElementRef<HTMLCanvasElement>): void {
    this.canvas = canvas.nativeElement;

    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      alpha: true,
      antialias: true
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    this.scene = new THREE.Scene();

    this.addCamera();

    this.addLight(this.lightColor1, this.lightPosition1);
    if (this.isNeedControl) {
      this.addOrbitControl();
    }
    // todo resolve issues in TS with material https://github.com/mrdoob/three.js/issues/12892
    // this.addDragControl();
    // this.addGroup();
    this.add3DAxes();
    this.addBox();
    this.addSphere();
  }
  private addCamera(): void {
    this.camera = new THREE.PerspectiveCamera(
      750, window.innerWidth / window.innerHeight, 0.1, 1000
    );
    this.camera.position.x = 4;
    this.camera.position.y = 3;
    this.camera.position.z = 5;
    this.scene.add(this.camera);
  }
  private addLight(color, position): void {
    this.light = new THREE.AmbientLight( color );
    this.light.position.z = position;
    this.light.castShadow = true;
    this.scene.add(this.light);
  }
  private addOrbitControl(): void {
    this.controls = new OrbitControls( this.camera, this.renderer.domElement );
  }
  private add3DAxes(): void {
    const dir1 = new THREE.Vector3( 1, 0, 0 );
    const dir2 = new THREE.Vector3( 0, 1, 0 );
    const dir3 = new THREE.Vector3( 0, 0, 1 );

    dir1.normalize();

    const origin = new THREE.Vector3( 0, 0, 0 );
    const length = 1.6;
    const hex = 0x000000;
    const headLength = 0.1;
    const headWidth = 0.02;

    const arrowHelper1 = new THREE.ArrowHelper( dir1, origin, length, hex, headLength, headWidth );
    const arrowHelper2 = new THREE.ArrowHelper( dir2, origin, length, hex, headLength, headWidth );
    const arrowHelper3 = new THREE.ArrowHelper( dir3, origin, length, hex, headLength, headWidth );
    this.scene.add( arrowHelper1 );
    this.scene.add( arrowHelper2 );
    this.scene.add( arrowHelper3 );
  }
  private addGroup(): void {
    this.group = new THREE.Group();
    this.scene.add( this.group );
  }
  private addBox(): void {
    const mesh = this.createBox(this.cubeSizes1);
    this.addItem(mesh, true);
  }
  private addSphere(): void {
    const mesh = this.createSphere(this.sphereSizes1);
    this.addItem(mesh, true);
  }
  private createBox(sizes, center = [1, 1, 1]): THREE.Mesh {
    const geometry = new THREE.BoxGeometry(...sizes);
    const material = new THREE.MeshNormalMaterial();
    // todo add move center from 0 0 0
    return new THREE.Mesh( geometry, material );
  }
  private createSphere(sizes, center = [1, 1, 1]): THREE.Mesh {
    const material = new THREE.MeshBasicMaterial( {color: this.color1} );
    const geometry = new THREE.SphereGeometry(...sizes);
    // todo add move center from 0 0 0
    return new THREE.Mesh( geometry, material );
  }
  private addItem(meshItem, isReused): void {
    if (isReused) {
      this.sceneObjects.push(meshItem);
    }
    this.scene.add(meshItem);
  }

  public animate(): void {
    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }

      window.addEventListener('resize', () => {
        this.resize();
      });
    });
  }

  public render(): void {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });
    this.renderer.render(this.scene, this.camera);
  }

  public resize(): void {
    const width = window.innerWidth;
    const height = window.innerHeight;

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize( width, height );
  }

  public addDragControl(): void {
    const startHandler = () => {
      if (this.isNeedControl) { this.controls.enabled = false; }
    };
    const dropHandler = () => {
      if (this.isNeedControl) { this.controls.enabled = true; }
    };
    this.dragControls = new DragControls( [...this.sceneObjects], this.camera, this.renderer.domElement );
    this.dragControls.addEventListener( 'drag', this.render );
    this.dragControls.addEventListener( 'dragstart', startHandler );
    this.dragControls.addEventListener( 'dragend', dropHandler );
    document.addEventListener( 'click', this.onClick, false );
  }
  private onClick(event): void {
    event.preventDefault();

    if ( this.enableSelection === true ) {

      const draggableObjects = this.dragControls.getObjects();
      draggableObjects.length = 0;

      this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
      this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

      this.raycaster.setFromCamera( this.mouse, this.camera );

      const intersections = this.raycaster.intersectObjects( draggableObjects, true );

      if ( intersections.length > 0 ) {

        const object = intersections[ 0 ].object;

        if ( this.group.children.includes( (object as THREE.Mesh) ) === true ) {

          // (object as THREE.Mesh).material.emissive.set( 0x000000 ); // https://github.com/mrdoob/three.js/issues/12892
          this.scene.attach( object );

        } else {

          // (object as THREE.Mesh).material.emissive.set( 0xaaaaaa );
          this.group.attach( object );

        }
        this.dragControls.transformGroup = true;
        draggableObjects.push( this.group );
      }

      if ( this.group.children.length === 0 ) {

        this.dragControls.transformGroup = false;
        draggableObjects.push( ...this.sceneObjects );
      }
    }

    this.render();
  }

  public destroyScene(): void {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
  }
}

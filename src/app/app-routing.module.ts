import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartComponent } from './views/start/start/start.component';

const routes: Routes = [
  { path: 'starting', component: StartComponent },
  {
    path: 'threejs',
    loadChildren: () => import('./views/lazyview3d/lazyview3d.module').then(m => m.Lazyview3dModule)
  },
  { path: '**',
    redirectTo: 'starting', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Lazyview3dRoutingModule } from './lazyview3d-routing.module';
import { Lazyview3dComponent } from './lazyview3d/lazyview3d.component';
import { EngineComponent } from '../../3d/engine/engine.component';


@NgModule({
  declarations: [Lazyview3dComponent, EngineComponent],
  imports: [
    CommonModule,
    Lazyview3dRoutingModule
  ]
})
export class Lazyview3dModule { }

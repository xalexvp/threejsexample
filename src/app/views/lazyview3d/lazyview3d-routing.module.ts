import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Lazyview3dComponent } from './lazyview3d/lazyview3d.component';

const routes: Routes = [
  {
    path: '',
    component: Lazyview3dComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Lazyview3dRoutingModule { }

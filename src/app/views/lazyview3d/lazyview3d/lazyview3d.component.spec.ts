import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lazyview3dComponent } from './lazyview3d.component';

describe('Lazyview3dComponent', () => {
  let component: Lazyview3dComponent;
  let fixture: ComponentFixture<Lazyview3dComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lazyview3dComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lazyview3dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

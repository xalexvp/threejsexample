import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StartComponent } from './start/start.component';

@NgModule({
  declarations: [StartComponent],
  exports: [StartComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class StartModule { }
